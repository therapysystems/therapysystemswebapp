const express = require('express');
const app = express();
const port = 8080;
const router = express.Router();
const bodyParser = require('body-parser');
const compression = require('compression');
const file = require('fs');
var isTraining = false;
var IMUData = {
    sensorData: [
        {
            sID: "sensor1",
            accelx: 0,
            accely: 0,
            accelz: 0,
            accelq: 0,
            gyrox: 0,
            gyroy: 0,
            gyroz: 0,
            gyroh: 0
        },
        {
            sID: "sensor2",
            accelx: 0,
            accely: 0,
            accelz: 0,
            accelq: 0,
            gyrox: 0,
            gyroy: 0,
            gyroz: 0,
            gyroh: 0
        }
    ]
}
var state = {
    curXAngle: 0,
    curYAngle: 0,
    curZAngle: 0,
};

var dataRT = {sensorData: [{
    sID: "Sensor1",
    x: 0,
    y: 0,
    z: 0,
}, {
    sID: "Sensor2",
    x: 0,
    y: 0,
    z: 0,
}]};


var count = 0;
var trainingData = [];
const raw = file.readFileSync("excercise.json");
const savedData = JSON.parse(raw);
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/assets', express.static(__dirname + '/assets'));
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/main.html');
});
app.post('/data', async function (req, res) {
    // console.log(JSON.stringify(state));
    dataRT = await filterData(req.body, state);
    console.log(dataRT);
    setIMUData(dataRT);
    if (isTraining) {
        trainingData.push(dataRT);
    }
    count = count + 1;
    res.json({ status: "success" });
});
app.get('/data', function (req, res) {
    if (isTraining == true) {
        res.json(getIMUData());
    } else {
        var data = getIMUData();
        var resObj = [];
        // console.log("Live data is " + JSON.stringify(data));
        resObj.push(data);
        // console.log(savedData[count]);
        if (savedData[count] == undefined) {
            count = 0;
        }
        else {
            resObj.push(savedData[count]);
        }
        // console.log("Saved data is " + savedData[count]);
        res.json(resObj);
        // console.log(JSON.stringify(resObj));
    }
});
app.post('/train', function (req, res) {
    isTraining = !isTraining;
    console.log(JSON.stringify(isTraining));
    res.json({ 'status': 'success' });
});
app.get('/finish', function (req, res) {
    console.log(isTraining);
    if (isTraining) {
        // save data for use later
        writeDataAsTextFile();
    }
    res.json({ status: "Done" });
    count = 0;
});
app.use('/', router);
app.listen(port, () => console.log(`Example app listening on port ${port}!`));

function setIMUData(data) {
    // console.log('dat is' +  JSON.stringify(data));
    dataRT = data;
}

function getIMUData() {
    return dataRT;
}

function writeDataAsTextFile() {
    console.log("Wrote to file");
    file.writeFile('./excercise.json', JSON.stringify(trainingData), function (err) {
        if (err) throw err;
        console.log('Updated!');
    });
}

async function filterData(data, state) {
    const sData = data.sensorData;
    // console.log(JSON.stringify(sData));
    var filteredData = [];
    for (var i = 0; i < sData.length; i++) {
        var gyroX = sData[i].gyrox;
        var gyroY = sData[i].gyroy;
        var gyroZ = sData[i].gyroz;
        var xdata = sData[i].accelx;
        var ydata = sData[i].accely;
        var zdata = sData[i].accelz;
        var dt = 0.100;//Time delay between each point
        var GyroBias = .98;
        var accelBias = .02;
        var xAngleFiltered = await GyroBias * (state.curXAngle + gyroX * dt) + accelBias * xdata;
        var yAngleFiltered = await GyroBias * (state.curYAngle + gyroY * dt) + accelBias * ydata;
        var zAngleFiltered = await GyroBias * (state.curZAngle + gyroZ * dt) + accelBias * zdata;
        state.curXAngle = xAngleFiltered;
        state.curYAngle = yAngleFiltered;
        state.curZAngle = zAngleFiltered;
        var fData = { sID: "Sensor" + (i + 1).toString(), x: xAngleFiltered, y: yAngleFiltered, z: zAngleFiltered }
        filteredData.push(fData);
    }
    // console.log(JSON.stringify(filteredData));
    return filteredData;
}